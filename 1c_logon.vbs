Option Explicit
Const Server = "nte-sh" ' Здесь мы зададим ip нашего общего ресурса (можно задать через имя компьютера)

If Ping (Server) = True Then ' проверим доступно ли место
Dim ProductCod 'код продукта (Смотри в каталоге каждого дистрибутива платформы в файле setup.ini
Dim DistrFolder 'каталог в которм находится дистрибутив платформы
Dim cmdLine 'Параметры запуска дистрибутива

'Деинсталируем предыдущие платформы
cmdLine = "REMOVE=ALL"

ProductCod = "{375C3BB1-6F1E-4D93-9AF7-6A379133CA81}"
DistrFolder= "\\" & Server & "\1s\8.2.10.73\"
	If Ustanovlena(ProductCod) Then ' Здесь и далее идет проверка на наличие установленной данной версии платформы
    	SdelatEto ProductCod, DistrFolder + "1CEnterprise 8.2.msi", cmdLine
	    'Иногда папка остается в каталоге installer после удаления - исправим это
	    UdalitFolder ProductCod
	End If

ProductCod = "{8150646B-4F88-4415-AF2A-F96199E3DA37}"
DistrFolder= "\\" & Server & "\1s\8.2.10.77\"
	If Ustanovlena(ProductCod) Then 
    	SdelatEto ProductCod, DistrFolder + "1CEnterprise 8.2.msi", cmdLine
	End If

'Установим текущую платформу
ProductCod = "{3D5173F0-371D-4B5D-B96A-9340D7F30431}"
DistrFolder= "\\" & Server & "\1s\8.2.10.82\"
cmdLine = "TRANSFORMS=adminstallrelogon.mst;1049.mst DESIGNERALLCLIENTS=0 THICKCLIENT=0 THINCLIENTFILE=1 THINCLIENT=1 WEBSERVEREXT=0 SERVER=0 CONFREPOSSERVER=0 CONVERTER77=0 SERVERCLIENT=0 LANGUAGES=RU" 
'В данном случае мне нужны только установленные тонкие клиенты, если надо больше - поменяйте на 1, то что надо установить   
	If Not Ustanovlena(ProductCod) Then ' Если текущая версия не установлена - установим ее
    	SdelatEto ProductCod, DistrFolder + "1CEnterprise 8.2.msi", cmdLine
	End If	
ScopirovatSpisokBaz ' В любом случае обновим список ИБ (Вдруг или мы или пользователи, что-то поменяли)
End If  

Sub SdelatEto (ByVal productCode, ByVal msiPackage, ByVal CmdLine)
    On Error Resume Next
    Dim installer
    Set installer = Nothing
    Set installer = Wscript.CreateObject("WindowsInstaller.Installer")
    installer.UILevel = 2 'молчаливая установка (варианты смотрите в Руководстве администратора 8.2 стр.185
    installer.InstallProduct msiPackage,cmdLine
    Set installer = Nothing
End Sub

Sub UdalitFolder (ProductCod)
    On Error Resume Next
    Dim fso1, WSH1
    set WSH1 = WScript.CreateObject("WScript.Shell")
    Set fso1 = WScript.CreateObject("Scripting.FileSystemObject")
    fso1.DeleteFolder WSH1.ExpandEnvironmentStrings("%systemroot%") & "\Installer\" & ProductCod, True
End Sub

'Создаем список баз данных (копируем наш фал с сервера на клиент
Sub ScopirovatSpisokBaz
    On Error Resume Next
    Dim fso1, WSH1
    Set WSH1 = WScript.CreateObject("WScript.Shell")
    Set fso1 = WScript.CreateObject("Scripting.FileSystemObject")
    fso1.CopyFile "\\" & Server & "\1s\1CEStart\ibases.v8i",WSH1.ExpandEnvironmentStrings("%AppData%") & "\1C\1CEStart\ibases.v8i", True
End Sub

'создание ярлыка
Sub createShurtcut
    Dim WshShell, oShellLink, ServerBD, NameBD
    ServerBD = "IZO-NoteBook"
    NameBD = "1C_Dec_82" 
    Set WshShell = WScript.CreateObject("WScript.Shell")
    Dim strDesktop : strDesktop = WshShell.SpecialFolders("Desktop")
    Set oShellLink = WshShell.CreateShortcut(strDesktop & "\" & shortcutName & ".lnk")
    oShellLink.TargetPath = WSHShell.ExpandEnvironmentStrings("%ProgramFiles%") & "\1cv82\Common\1cestart.exe /S" & ServerBD & "\" & NameBD 'Если не указывать параметры, то будет появлятся стандартное окно выбора ИБ, если укажем, то сразу имя пользователя-пароль )
    oShellLink.WindowStyle = 1
    oShellLink.Description = shortcutName
    oShellLink.Save 
    Set oShellLink = Nothing
    Set WshShell = Nothing
End Sub

Function Ustanovlena(ProductCod)
    On Error Resume Next
    Dim fso1, oFolders, WSH1
    set WSH1 = WScript.CreateObject("WScript.Shell")
    Set fso1 = WScript.CreateObject("Scripting.FileSystemObject")
    Set oFolders = Nothing
    Set oFolders = fso1.GetFolder(WSH1.ExpandEnvironmentStrings("%systemroot%") & "\Installer\" & ProductCod)
    
    If OFolders  Is Nothing Then
        Ustanovlena = False
    Else
        Ustanovlena = True
    End If
End Function

Function Ping(strHost)
' Просто пингуем наш компутер, проверяя включен ли он
        Dim objPing, objRetStatus
        Set objPing = GetObject("winmgmts:\\.\root\CIMV2").ExecQuery _
        ("S_elect * FROM Win32_PingStatus WHERE address = '" & strHost & "'")
        for Each objRetStatus in objPing
          if IsNull(objRetStatus.StatusCode) or objRetStatus.StatusCode<>0 Then
            Ping = False
          Else
            Ping = True
          End If
        Next
End Function    